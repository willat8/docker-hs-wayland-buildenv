#FROM willat8/wayland-hs-buildenv-debian-rpi3:20170720

#RUN apt-get update && apt-get install -y --no-install-recommends librsvg2-dev alex happy \
# && rm -rf /var/lib/apt/lists/*

#RUN cabal install --global svgcairo

#FROM willat8/hs-wayland-buildenv:20210501

#RUN cabal install --global http
#RUN cabal install --global --allow-newer snmp

#FROM willat8/hs-wayland-buildenv:20230121

#COPY qemu-aarch64-static /run/binfmt/aarch64

FROM willat8/hs-wayland-buildenv:20231008

RUN cp /root/.cabal/packages/hackage.haskell.org/00-index.* /tmp \
 && cabal update \
 && cabal install --global --reinstall memory-0.14.18 cryptonite-0.25 basement-0.0.7 tls-1.5.0 http-client-tls-0.2.4.1 http-conduit-2.1.11 \
 && mv /tmp/00-index.* /root/.cabal/packages/hackage.haskell.org
